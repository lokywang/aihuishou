const api = require('../../utils/api.js');
const utils = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name:'',
    MEMBER_CODE: ''
  },
  //确认修改
  querenxiugaiAction: function (e) {
    //确认修改昵称接口
    var self = this;
    if (self.data.name.length < 6) {
      wx: wx.showToast({
        title: '请填写6-18位密码',
        icon: 'none',
        mask: true,
      })
    } else {
      self.getdataUP();
    }
  },

  //获取方法 
  getdataUP: function (e) {
    var self = this;
    wx.showLoading({
      title: '正在设置...',
      mask: true
    });
    var canshu = {
      MEMBER_CODE: self.data.MEMBER_CODE,
      PASSWORD: self.data.name,
    };
    //进行数据请求
    utils.request(api.PostEditPassword, canshu).then(res => {

      console.log(res);
      if (res.WS_RET_CODE == 0) {
        //请求成功
        wx: wx.showToast({
          title: '密码设置成功',
          icon: 'success',
          mask: true,
        })
        wx.navigateBack();
      }else{
        
        wx: wx.showToast({
          title: res.WS_RET_MSG,
          icon: 'none',
          mask: true,
        })
      }
       
      wx.hideLoading();
    }).catch((err) => {
      console.log(err);
      wx.hideLoading();
    });
  },


  //输入的方法
  inputyanzhengma: function (e) {
    var self = this;
    // 获取输入框的内容  
    var value = e.detail.value;
    self.setData({
      name: value
    })

  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      MEMBER_CODE: options.MEMBER_CODE
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})