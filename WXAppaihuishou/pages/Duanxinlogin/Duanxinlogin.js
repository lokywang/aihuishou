const api = require('../../utils/api.js');
const utils = require('../../utils/util.js');
var app = getApp();
//引入通知
var WxNotificationCenter = require('../../vendors/WxNotificationCenter.js')
var interval = null //倒计时函数
Page({

  /**
   * 页面的初始数据
   */
  data: {
      iphone:'',
      yanzhengma:'',
      datatime:0,
      time:'发送验证码',
      isabled:true,
      WS_RET_DATA:{},
  },
 
  //手机号码输入的方法
  inputiphone:function(e){
    var self = this;
    // 获取输入框的内容  
    var value = e.detail.value;
    self.setData({
      iphone: value
    })
  },

  //验证码输入的方法
  inputyanzhengma:function(e){
    var self=this;
    // 获取输入框的内容  
    var value = e.detail.value;
    self.setData({
      yanzhengma: value
    })


  },

  //切换方法
  mimadengloAction:function(e){
    //密码登录换方法
    wx.redirectTo({
      url: '../Loginview/Loginview',
    })
  },

  //发送验证码
  fasongyanzhengmaAction:function(e){
     var self=this;
     

     if (self.data.iphone.length != 11) {
       wx.showModal({
         title: '提示',
         content: '格式不正确,请输入正确手机号',
         showCancel: false,
         success: function (res) {
         }
       })
       return;
     }

     //判断是否可点击
     if (self.data.isabled == true){
          //证明可以点击
          
       wx.showLoading({
         title: '正在发送验证码...',
         mask: true
       });

       utils.request(api.PostSendSmsCode, { MOBILE: self.data.iphone }).then(res => {
         console.log(res.WS_RET_DATA);
         if (res.WS_RET_DATA==null){
           wx.showModal({
             title: '提示',
             content: res.WS_RET_MSG,
             showCancel:false,
             success: function (res) {
             }
           })
           wx.hideLoading();
           return;
         }

         self.setData({
           datatime: res.WS_RET_DATA,
           isabled: false
         })

         var currentTime = self.data.datatime
         interval = setInterval(function () {
           currentTime--;
           self.setData({
             time: currentTime + '秒'
           })
           if (currentTime <= 0) {
             clearInterval(interval)
             self.setData({
               time: '重新获取',
               currentTime: 0,
               isabled: true
             })
           }
         }, 1000)
         wx:wx.hideLoading();
       }).catch((err) => {
         console.log(err);
         wx: wx.hideLoading();
       });


     }else{
       //不可点击
       return
     }

     
  },

  //登录方法
  dengluAction:function(e){
      var self=this;

      if (self.data.yanzhengma.length <= 0) {
        wx.showModal({
          title: '提示',
          content: '验证码不能为空',
          showCancel: false,
          success: function (res) {
          }
        })
        return;
      }

      wx.showLoading({
        title: '正在登录...',
        mask: true
      });

      var canshudata={
        LOGIN_TYPE:1,
        MOBILE: self.data.iphone,
        PASSWORD: self.data.yanzhengma,
        OPEN_ID: app.globalData.openidstr
      };
      utils.request(api.PostLogin, canshudata).then(res => {

        if (res.WS_RET_CODE==1){
          wx.hideLoading();
          wx.showModal({
            title: '提示',
            content: res.WS_RET_MSG,
            showCancel: false,
            success: function (res) {
            }
          })
          return;
        }else{

           //关闭倒计时
          clearInterval(interval)

          // console.log(res.WS_RET_DATA);
          /**
           * | MEMBER_CODE | long | 用户ID
             | IS_REGISTER | bool | 是否新注册 true:是，false:否
             | IS_PUBLISH | bool | 是否有权限发布 true:有,false:没有
             IS_HAD_PASSWORD 是否设置过登录密码  没有就跳转到设置密码页面
          */
          self.setData({
            WS_RET_DATA: res.WS_RET_DATA
          })
           
          // //获取页面栈
          // var pages = getCurrentPages();
          // if (pages.length > 1) {
          //   //上一个页面实例对象
          //   var prePage = pages[pages.length - 2];
          //   if (prePage.route == "pages/Myview/Myview") {
          //     //如果等于个人中心 则回到个人中心 去更新数据
          //     //关键在这里
          //     prePage.quanxianAction();
          //   }
          // }

          //发送通知（所有注册过'NotificationName'的页面都会接收到通知）
          WxNotificationCenter.postNotificationName('MyviewUP', null)

          //判断是否已经设置过密码
          if (res.WS_RET_DATA.IS_HAD_PASSWORD==false){
               //没有 跳转到设置密码页面
            //短信验证切换方法
            wx.redirectTo({
              url: '../SetMima/SetMima?MEMBER_CODE=' + res.WS_RET_DATA.MEMBER_CODE,
            })
          }else{
            
            wx.navigateBack();
          }

        }
        wx.hideLoading();
    }).catch((err) => {
        console.log(err);
        wx.hideLoading();
    });

  },

// 进入协议方法
  goxieyiAction:function(){
    // Xieyiview / Xieyiview
    wx.navigateTo({
      url: '../Xieyiview/Xieyiview',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})