const api = require('../../utils/api.js');
const utils = require('../../utils/util.js');
var app = getApp();
//引入通知
var WxNotificationCenter = require('../../vendors/WxNotificationCenter.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
     islogin:true,
     MEMBER_CODE:'',
     page_num:0,
     page_step:10,
     gongsimingcheng:'',
     MEMBER_INFO:{},
     listdataarray: [],
     TOTAL_RECORDS:0,
     time:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
     //第一次进来进行权限的验证 并获取member-code
     var that = this;
     that.quanxianAction();
     //注册通知
     //取出当天时间
     // 调用函数时，传入new Date()参数，返回值是日期和时间  
     var time = utils.formatTime(new Date());
     // 再通过setData更改Page()里面的data，动态更新页面的数据  
     // console.log(time);
     that.setData({
       time: time
     });  

     WxNotificationCenter.addNotification('MyviewUP', that.didNotification, that)
  },

  //通知处理
  didNotification: function (info) {
    //更新数据
    var self = this;
    self.quanxianAction();
  },
   

  //判断权限方法
  quanxianAction: function () {
    var self=this;
    var canshu = {
      OPEN_ID: app.globalData.openidstr
    };
    //进行发布的判断全限
    utils.request(api.PostValidBindMobile, canshu).then(res => {
      console.log(res);

      if (res.WS_RET_DATA.MEMBER_CODE == -1) {
        
        self.setData({
          islogin:false,
          MEMBER_CODE: res.WS_RET_DATA.MEMBER_CODE
        })

      } else {
        //登陆过  获取个人信息
        self.setData({
          islogin: true,
          MEMBER_CODE: res.WS_RET_DATA.MEMBER_CODE,
          page_num: 0,
          TOTAL_RECORDS:0,
        })

        self.getdataUP();
      }
    }).catch((err) => {
      console.log(err);
    });

  },

//获取信息方法 以及列表数据拉去
 getdataUP:function(e){
   var self=this;
   wx.showLoading({
     title: '正在加载...',
     mask: true
   });
   var canshu = {
     member_code:self.data.MEMBER_CODE,
     page_num: self.data.page_num,
     page_step: self.data.page_step,
   };
   //进行数据请求
   utils.request(api.LoadMyNews, canshu).then(res => {

     console.log(res);
     if (res.WS_RET_CODE==0){
       //请求成功
       var complany='';
       if (res.WS_RET_DATA.MEMBER_INFO.COMPANY_NAME.length==0){
          complany='无公司';
       }else{
         complany = res.WS_RET_DATA.MEMBER_INFO.COMPANY_NAME;
       }

      //  self.setData({
      //    MEMBER_INFO: res.WS_RET_DATA.MEMBER_INFO,
      //    gongsimingcheng: complany
      //  })
       
       for (var i = 0; i < res.WS_RET_DATA.DATA.length; i++) {
         var ordtime = res.WS_RET_DATA.DATA[i].CREATE_TIME_NEW.substring(0, 10)
         if (ordtime == self.data.time) {
           res.WS_RET_DATA.DATA[i].CREATE_TIME_NEW = res.WS_RET_DATA.DATA[i].CREATE_TIME_NEW.replace(ordtime, '今天')
         }
       }

       if (self.data.page_num == 0) {
         self.setData({
           listdataarray: res.WS_RET_DATA.DATA,
           TOTAL_RECORDS: res.WS_RET_DATA.TOTAL_RECORDS,
           MEMBER_INFO: res.WS_RET_DATA.MEMBER_INFO,
           gongsimingcheng: complany
         })
       } else {
         var morearray = self.data.listdataarray;
         var moredata = morearray.concat(res.WS_RET_DATA.DATA);
         self.setData({
           listdataarray: moredata,
           TOTAL_RECORDS: res.WS_RET_DATA.TOTAL_RECORDS,
           MEMBER_INFO: res.WS_RET_DATA.MEMBER_INFO,
           gongsimingcheng: complany
         })

       }
       wx.hideLoading();
       wx.stopPullDownRefresh();

     }  
   
   }).catch((err) => {
     wx.hideLoading();
     wx.stopPullDownRefresh();
     console.log(err);
   });


 },

 //去登录
  gologinAction:function(e){
    //未登录
    wx.navigateTo({
      url: '../Loginview/Loginview',
    })
  },


  //置顶列表数据
  zhidingAction: function () {
    wx.pageScrollTo({
      scrollTop: 0
    })
  },


  //进入发布
  fabuAction: function () {

    var canshu = {
      OPEN_ID: app.globalData.openidstr
    };
    //进行发布的判断全限
    utils.request(api.PostValidBindMobile, canshu).then(res => {
      console.log(res);

      if (res.WS_RET_DATA.MEMBER_CODE == -1) {
        //未登录
        wx.navigateTo({
          url: '../Loginview/Loginview',
        })

      } else {
        if (res.WS_RET_DATA.IS_PUBLISH == false) {
          //登陆过 但没有权限
          wx.navigateTo({
            url: '../Nopermiss/Nopermiss',
          })

        } else {
          //登陆过 而且有权限
          wx.navigateTo({
            url: '../Addview/Addview?MEMBER_CODE=' + res.WS_RET_DATA.MEMBER_CODE,
          })
        }
      }
    }).catch((err) => {
      console.log(err);
    });
  },


  /**   
      * 预览图片  
      */
  previewImage: function (e) {
    var current = e.target.dataset.src;
    var index = e.target.dataset.ind;//当前段落

    wx.previewImage({
      current: current, // 当前显示图片的http链接  
      urls: this.data.listdataarray[index].IMGS // 需要预览的图片http链接列表  
    })
  },

 //进入设置中心
  shezhiviewAction:function(e){
    wx.navigateTo({
      url: '../Setview/Setview?MEMBER_CODE=' + this.data.MEMBER_CODE,
    }) 
  },

//删除订单
  deleteOrderAction:function(e){
    var self=this;
     
    wx.showModal({
      title: '提示',
      content: '您确定要删除这条内容吗？',
      success: function (res) {
        if (res.confirm) {
          wx.showLoading({
            title: '正在删除...',
            mask: true
          });
          var ind = e.target.dataset.ind;
          var NEW_IDstr = e.target.dataset.new_id;
          var canshu = {
            MEMBER_CODE: self.data.MEMBER_CODE,
            NEW_ID: NEW_IDstr
          };
          utils.request(api.PostDeleteNews, canshu).then(res => {
            wx.hideLoading();
            if (res.WS_RET_CODE == 0) {
              wx.showToast({
                title: '删除成功',
                icon: 'success',
                mask: true,
              })
              //从数据源删除item
              var listarray = self.data.listdataarray;
              listarray.splice(ind,1);
              self.setData({
                listdataarray: listarray
              })
          
            } else {
               wx.showToast({
                title: res.WS_RET_MSG,
                icon: 'none',
                mask: true,
              })
            }
          }).catch((err) => {
            console.log(err);
            wx.hideLoading();
          });
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })  

  },
 

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },


  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    //移除通知
    var that = this
    WxNotificationCenter.removeNotification('NotificationName', that)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    that.setData({
      page_num: 0
    })
    that.quanxianAction();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;

    //判断是不是可以再次刷新
    if (that.data.TOTAL_RECORDS <= that.data.listdataarray.length) {
      wx.showToast({
        title: '没有更多数据',
        icon: 'success',
        duration: 1000,
        mask: true
      });
      return;
    }

    var num = that.data.page_num + 1;
    that.setData({
      page_num: num
    })
    that.getdataUP();
  },

})