// pages/Addview/Addview.js
var app = getApp();
const api = require('../../utils/api.js');
const utils = require('../../utils/util.js');
//引入通知
var WxNotificationCenter = require('../../vendors/WxNotificationCenter.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    min: 0,//最少字数  
    max: 150, //最多字数 (根据自己需求改变) 
    tempFilePaths: [],//存放选择后的图片
    neirong: '',
    currentWordNumber:0,
    iscantijiao:false,
    MEMBER_CODE:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      MEMBER_CODE: options.MEMBER_CODE
    })
  },
   

  //字数限制    
  inputs: function (e) {
     
     //禁止输入表情
    var ranges = [
      '\ud83c[\udf00-\udfff]',
      '\ud83d[\udc00-\ude4f]',
      '\ud83d[\ude80-\udeff]'
    ];

    e.detail.value = e.detail.value.replace(new RegExp(ranges.join('|'), 'g'), '');
    
    // 获取输入框的内容  
    var value = e.detail.value;

    // 获取输入框内容的长度  
    var len = parseInt(value.length);
    //最多字数限制  
    if (len > this.data.max) return;
    // 当输入框内容的长度大于最大长度限制（max)时，终止setData()的执行  
    this.setData({
      currentWordNumber: len, //当前字数 
      neirong: value,
    });
    
    if (this.data.currentWordNumber > 0 || this.data.tempFilePaths.length > 0) {
      this.setData({
        iscantijiao: true,
      });
    } else {
      this.setData({
        iscantijiao: false,
      });
    }


  },

  //点击上传
  shangchuanimageBut: function (e) {
    var that = this;
    var imagelength = that.data.tempFilePaths.length;
    wx.chooseImage({
      count: 9 - imagelength, // 默认9  
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有  
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有  
      success: function (res) {
        console.log(res);
        var array = that.data.tempFilePaths;
        array = array.concat(res.tempFilePaths);

        console.log(array);

        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片  
        that.setData({
          tempFilePaths: array
        })
        if (that.data.currentWordNumber > 0 || that.data.tempFilePaths.length > 0) {
          that.setData({
            iscantijiao: true,
          });
        } else {
          that.setData({
            iscantijiao: false,
          });
        }
      }
    })
  },

  //删除图片方法
  shangchuBut: function (e) {
    var that = this;
    const index = e.currentTarget.dataset.index; //得到index的值
    console.log(index);
    var notes = that.data.tempFilePaths;
    notes.splice(index, 1);//根据索引删除指定元素个数

    that.setData({
      tempFilePaths: notes
    })
    if (that.data.currentWordNumber > 0 || that.data.tempFilePaths.length > 0) {
      that.setData({
        iscantijiao: true,
      });
    } else {
      that.setData({
        iscantijiao: false,
      });
    }
  },


  /**   
    * 预览图片  
    */
  previewImage: function (e) {
    var current = e.target.dataset.src;
    wx.previewImage({
      current: current, // 当前显示图片的http链接  
      urls: this.data.tempFilePaths // 需要预览的图片http链接列表  
    })
  },

  //点击发布按钮方法
  fabuAction:function(e){
    var self = this;

    if (self.data.neirong.length <= 0 && self.data.tempFilePaths.length <= 0) {

      wx.showModal({
        title: '提示',
        content: '请完善提交内容',
        showCancel: false,
        success: function (res) {
          if (res.confirm) {
            console.log('用户点击确定')
          }

        }
      })

      return;
    }

    // PostUploadImg
    if (self.data.tempFilePaths.length > 0) {

      if (self.data.neirong.length <= 0){
        wx.showModal({
          title: '提示',
          content: '请填写发布内容',
          showCancel: false,
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            }

          }
        })

        return;
      }
      wx.showLoading({
        title: '正在提交图片...',
        mask: true
      });

      utils.uploadimg({
        url: api.PostUploadImg,//这里是你图片上传的接口
        path: self.data.tempFilePaths,//这里是选取的图片的地址数组
        pic: [],
      }, function (res) {
        console.log(res);
        wx.hideLoading();

        var canshu={
          member_code: self.data.MEMBER_CODE,
          content: self.data.neirong,
          img: res
        }; 
        
        wx.showLoading({
          title: '正在发布...',
          mask: true
        });


        utils.request(api.PostSaveNews, canshu).then(res => {

          if (res.WS_RET_CODE == 1) {
            wx.showModal({
              title: '提示',
              content: res.WS_RET_MSG,
              showCancel: false,
              success: function (res) {
              }
            })
            wx.hideLoading();
            return;
          }


          //发布成功
          wx.showModal({
            title: '发布成功',
            content: '客服人员审核通过后将在首页中展示',
            showCancel: false,
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              }
            }
          })
          //发送通知（所有注册过'NotificationName'的页面都会接收到通知）
          WxNotificationCenter.postNotificationName('MyviewUP', null)
          wx.hideLoading();
          wx.navigateBack();
        }).catch((err) => {
          console.log(err);
          wx.hideLoading();
        });

      });
    
    }else{
      var canshu = {
        member_code: self.data.MEMBER_CODE,
        content: self.data.neirong,
      };
      wx.showLoading({
        title: '正在发布...',
        mask: true
      });
      utils.request(api.PostSaveNews, canshu).then(res => {

        if (res.WS_RET_CODE == 1) {
          wx.showModal({
            title: '提示',
            content: res.WS_RET_MSG,
            showCancel: false,
            success: function (res) {
            }
          })
          wx.hideLoading();
          return;
        }

        //发布成功
        wx.showModal({
          title: '发布成功',
          content: '客服人员审核通过后将在首页中展示',
          showCancel: false,
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            }
          }
        })
        //发送通知（所有注册过'NotificationName'的页面都会接收到通知）
        WxNotificationCenter.postNotificationName('MyviewUP', null)
        wx.navigateBack();
        wx.hideLoading();
      }).catch((err) => {
        console.log(err);
        wx.hideLoading();
      });
    }
   
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})