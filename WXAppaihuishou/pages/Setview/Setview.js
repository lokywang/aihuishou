const api = require('../../utils/api.js');
const utils = require('../../utils/util.js');
var app = getApp();
//引入通知
var WxNotificationCenter = require('../../vendors/WxNotificationCenter.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    MEMBER_CODE:'',
    WS_RET_DATA:{},
    gongsimingcheng:'',
    xuanzetupainURl:'../../resources/huiseper.png',
  },


  //获取信息方法 
  getdataUP: function (e) {
    var self = this;
    wx.showLoading({
      title: '正在刷新...',
      mask: true
    });
    var canshu = {
      member_code: self.data.MEMBER_CODE,
    };
    //进行数据请求
    utils.request(api.PostLoadMemberInfo, canshu).then(res => {

      console.log(res);
      if (res.WS_RET_CODE == 0) {
        //请求成功
        var complany = '';
        if (res.WS_RET_DATA.COMPANY_NAME.length == 0) {
          complany = '无公司';
        } else {
          complany = res.WS_RET_DATA.COMPANY_NAME;
        }
        var imagurl = res.WS_RET_DATA.ICON;
        if(imagurl.length==0){
          imagurl ='../../resources/huiseper.png'
        }
        self.setData({
          WS_RET_DATA: res.WS_RET_DATA,
          gongsimingcheng: complany,
          xuanzetupainURl: imagurl
        })
      }
      wx.hideLoading();
    }).catch((err) => {
      wx.hideLoading();
      console.log(err);
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.MEMBER_CODE);
    var self=this;

    //注册通知
    WxNotificationCenter.addNotification('setviewUPdata', self.didNotification, self)

    self.setData({
      MEMBER_CODE: options.MEMBER_CODE
    })
    self.getdataUP();
  },

  //通知处理
  didNotification: function (info) {
    //更新数据
    var self = this;
    self.getdataUP();
  },

//修改昵称
  xiugainameAction:function(e){
    wx.navigateTo({
      url: '../Xiugainame/Xiugainame?name=' + this.data.WS_RET_DATA.NICK_NAME + '&MEMBER_CODE=' + this.data.MEMBER_CODE,
    })
  },

   
   //退出登录
  OutLoginAction:function(e){
    wx.showLoading({
      title: '正在退出...',
      mask: true
    });
    var self = this;
    var canshu = {
      MEMBER_CODE: self.data.MEMBER_CODE,
      OPEN_ID: app.globalData.openidstr,
    };
    //进行数据请求
    utils.request(api.PostCancelLogin, canshu).then(res => {
      // console.log(res);
      if (res.WS_RET_CODE == 0) {
          //退出成功
        // //获取页面栈
        // var pages = getCurrentPages();
        // if (pages.length > 1) {
        //   //上一个页面实例对象
        //   var prePage = pages[pages.length - 2];
        //   if (prePage.route == "pages/Myview/Myview") {
        //     //如果等于个人中心 则回到个人中心 去更新数据
        //     //关键在这里
        //     prePage.quanxianAction();
        //   }
        // }
        //发送通知（所有注册过'NotificationName'的页面都会接收到通知）
        WxNotificationCenter.postNotificationName('MyviewUP', null)
        wx.navigateBack();
      }
      wx.hideLoading();
    }).catch((err) => {
      console.log(err);
      wx.hideLoading();
    });

  },

  //点击上传
  shangchuanimageBut: function (e) {
    var that = this;
    wx.chooseImage({
      count: 1, // 默认9  
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有  
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有  
      success: function (res) {
        console.log(res);
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片  
        that.setData({
          xuanzetupainURl: res.tempFilePaths[0]
        })
        wx.showLoading({
          title: '正在加载...',
          mask: true
        });
        var datacanshu = { member_code: that.data.MEMBER_CODE};
        wx:wx.uploadFile({
          url: api.PostSaveIcon,
          filePath: that.data.xuanzetupainURl,
          name: 'file',
          formData: {
            z_data: JSON.stringify(datacanshu)
          },
          success: function(res) {
            //请求成功
            wx: wx.showToast({
              title: '头像修改成功',
              icon: 'success',
              mask: true,
            }) 
            //发送通知（所有注册过'NotificationName'的页面都会接收到通知）
            WxNotificationCenter.postNotificationName('MyviewUP', null)
          },
          fail: function(res) {},
          complete: function(res) {
            wx.hideLoading();
          },
        })
      }
    })


  },




})