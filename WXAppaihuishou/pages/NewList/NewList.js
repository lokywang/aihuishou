var WxSearch = require('../../wxSearchView/wxSearchView.js');
var app = getApp();
const api = require('../../utils/api.js');
const utils = require('../../utils/util.js');
var timer; // 计时器
Page({

  /**
   * 页面的初始数据
   */
  data: {
      page_num: 0,
      TOTAL_RECORDS:0,
      listdataarray:[],
      sousuostr:'',
      time:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
    this.countdown();

    //取出当天时间
    // 调用函数时，传入new Date()参数，返回值是日期和时间  
    var time = utils.formatTime(new Date());
    // 再通过setData更改Page()里面的data，动态更新页面的数据  
    // console.log(time);
    this.setData({
      time: time
    });  

   //判断openid 是否已经获取到
    if (app.globalData.openidstr == null){
      // 登录
      wx.login({
        success: res => {
          // 发送 res.code 到后台换取 openId, sessionKey, unionId

          utils.request(api.getopenidURL, { code: res.code }).then(res => {
            console.log(res.WS_RET_DATA.openID);
            //存储openid
            app.globalData.openidstr = res.WS_RET_DATA.OPEN_ID;

            this.getupdata();

          }).catch((err) => {
            console.log(err);
          });

        }

      })

    }else{
      this.getupdata();
    }

    
  },

//请求数据接口
  getupdata: function(){

    wx.showLoading({
      title: '正在加载...',
      mask: true
    });

    var that=this;
    var canshudata = {
      open_id: app.globalData.openidstr,
      page_num: that.data.page_num,
      page_step: 10,
      keyword: that.data.sousuostr
    }
    utils.request(api.shouyeshujuUrl, canshudata).then(res => {
      console.log(res.WS_RET_DATA);
     
      for (var i = 0; i < res.WS_RET_DATA.DATA.length; i++) {
        var ordtime = res.WS_RET_DATA.DATA[i].CREATE_TIME_NEW.substring(0, 10)
        if (ordtime == that.data.time) {
          res.WS_RET_DATA.DATA[i].CREATE_TIME_NEW=res.WS_RET_DATA.DATA[i].CREATE_TIME_NEW.replace(ordtime, '今天')
        }
      }
      
      if (that.data.page_num == 0){
        that.setData({
          listdataarray: res.WS_RET_DATA.DATA,
          TOTAL_RECORDS: res.WS_RET_DATA.TOTAL_RECORDS
        })
      }else{
        var morearray = that.data.listdataarray;
        var moredata= morearray.concat(res.WS_RET_DATA.DATA);
        that.setData({
          listdataarray: moredata
        })

      }
      wx.hideLoading();
      wx.stopPullDownRefresh();
    }).catch((err) => {
      wx.hideLoading();
      wx.stopPullDownRefresh();
      console.log(err);
    });

  },

  // 搜索入口  
  wxSearchTab: function () {
    wx.navigateTo({
      url: '../search/search'
    })
  },
  
  /**   
      * 预览图片  
      */
  previewImage: function (e) {
    var current = e.target.dataset.src;
    var index = e.target.dataset.ind;//当前段落

    wx.previewImage({
      current: current, // 当前显示图片的http链接  
      urls: this.data.listdataarray[index].IMGS // 需要预览的图片http链接列表  
    })
  },
 
  //取消按钮方法
  wxSearchConfirm:function (e){
    var that = this;
    //下拉清除角标
    wx: wx.removeTabBarBadge({
      index: 0,
    })

    that.setData({
      page_num: 0,
      sousuostr: '',
    })
    that.getupdata();
  },

  //置顶列表数据
  zhidingAction:function (){
    wx.pageScrollTo({
      scrollTop: 0
    })
  },
 

   //进入发布
  fabuAction:function (){
    
    var canshu={
      OPEN_ID: app.globalData.openidstr
    };
     //进行发布的判断全限
    utils.request(api.PostValidBindMobile, canshu ).then(res => {
       console.log(res);

       if (res.WS_RET_DATA.MEMBER_CODE==-1){
         //未登录
         wx.navigateTo({
           url: '../Loginview/Loginview',
         })

       }else{
         if (res.WS_RET_DATA.IS_PUBLISH == false){
           //登陆过 但没有权限
           wx.navigateTo({
             url: '../Nopermiss/Nopermiss',
           })
           
         }else{
           //登陆过 而且有权限
           wx.navigateTo({
             url: '../Addview/Addview?MEMBER_CODE=' + res.WS_RET_DATA.MEMBER_CODE,
           })
         }
       }
    }).catch((err) => {
      console.log(err);
    });

   

  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log('首页展示了');
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    // clearTimeout(timer);
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    console.log('页面销毁了');
    clearTimeout(timer);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
      var that = this;
      //下拉清除角标
      wx:wx.removeTabBarBadge({
        index: 0,
      })

      that.setData({
        page_num:0
      })
      that.getupdata();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    
    //判断是不是可以再次刷新
       
    if (that.data.TOTAL_RECORDS <= that.data.listdataarray.length){
      wx.showToast({
        title: '没有更多数据',
        icon: 'success',
        duration: 1000,
        mask: true
      });
      return;
    }
  
    var num = that.data.page_num+1;
    that.setData({
      page_num: num
    })
    that.getupdata();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  // 定时器 下面是毫秒数  没隔多长时间进行一次方法
  countdown :function () {
    var that=this;
    timer = setTimeout(function () {
      console.log('倒计时执行');
      utils.request(api.zuixinNewNUM, { open_id: app.globalData.openidstr }).then(res => {
        // console.log(app.globalData.openidstr + res.WS_RET_DATA.NEWS_NUM);
        if (res.WS_RET_DATA.NEWS_NUM>0){

           wx.setTabBarBadge({
             index: 0,
             text: res.WS_RET_DATA.NEWS_NUM+''
           })

        }

        that.countdown();

      }).catch((err) => {
        console.log(err);
      });

      
    }
      , 50000)
  }

})

