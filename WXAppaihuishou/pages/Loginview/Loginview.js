const api = require('../../utils/api.js');
const utils = require('../../utils/util.js');
var app = getApp();
//引入通知
var WxNotificationCenter = require('../../vendors/WxNotificationCenter.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    iphone: '',
    yanzhengma: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  //手机号码输入的方法
  inputiphone: function (e) {
    var self = this;
    // 获取输入框的内容  
    var value = e.detail.value;
    self.setData({
      iphone: value
    })
  },

  //密码输入的方法
  inputyanzhengma: function (e) {
    var self = this;
    // 获取输入框的内容  
    var value = e.detail.value;
    self.setData({
      yanzhengma: value
    })

  },
   
  duanxinyanzhengAction:function(e){
     //短信验证切换方法
    wx.redirectTo({
        url: '../Duanxinlogin/Duanxinlogin',
      })
  },

  //登录方法
  dengluAction: function (e) {
    var self = this;

    if (self.data.iphone.length !=11){
        wx.showModal({
          title: '提示',
          content: '格式不正确,请输入正确手机号',
          showCancel: false,
          success: function (res) {
          }
        })
        return;
    }
   
    if (self.data.yanzhengma.length<=0){
      wx.showModal({
        title: '提示',
        content: '密码不能为空',
        showCancel: false,
        success: function (res) {
        }
      })
      return;
    }
    wx.showLoading({
      title: '正在登录...',
      mask: true
    });
    var canshudata = {
      LOGIN_TYPE: 0,
      MOBILE: self.data.iphone,
      PASSWORD: self.data.yanzhengma,
      OPEN_ID: app.globalData.openidstr
    };
    utils.request(api.PostLogin, canshudata).then(res => {

      if (res.WS_RET_CODE == 1) {
        wx.showModal({
          title: '提示',
          content: res.WS_RET_MSG,
          showCancel: false,
          success: function (res) {
          }
        })
        wx.hideLoading();
        return;
      } else {
        // console.log(res.WS_RET_DATA);
        //发送通知（所有注册过'NotificationName'的页面都会接收到通知）
        WxNotificationCenter.postNotificationName('MyviewUP', null)

        wx.navigateBack();
      }
      wx.hideLoading();
      console.log(res);

    }).catch((err) => {
      wx.hideLoading();
      console.log(err);
    });

  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})