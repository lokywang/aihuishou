/**https://xcx.51bxg.com/api/ 正式环境*/ 
/**http://wx.bxg.cn/api/ 205环境环境*/

// var webUrl = "http://wx.bxg.cn/api/";//205
var webUrl = "https://xcx.51bxg.com/api/";//公网

module.exports = {
  getopenidURL: webUrl +"WxOpen/PostOnLogin?from=app", //换取openid接口
  shouyeshujuUrl: webUrl +"MiniNews/LoadIndexData?from=app",//首页数据接口
  zuixinNewNUM: webUrl + "MiniNews/LoadNewData?from=app",//最新数据条数
  PostUploadImg: webUrl + "MiniNews/PostUploadImg?from=app",//上传图片
  PostSaveNews: webUrl + "MiniNews/PostSaveNews?from=app",//发布新闻
  PostValidBindMobile: webUrl + "WxLogin/PostValidBindMobile?from=app",//根据OPEN_ID验证是否绑定过手机
  PostSendSmsCode: webUrl + "WxLogin/PostSendSmsCode?from=app",//短信验证码
  PostLogin: webUrl + "WxLogin/PostLogin?from=app",//登录接口
  LoadMyNews: webUrl + "MiniNews/LoadMyNews?from=app",//我的新闻
  PostLoadMemberInfo: webUrl + "MiniNews/PostLoadMemberInfo?from=app",//用户信息
  PostSaveNickName: webUrl + "MiniNews/PostSaveNickName?from=app",//修改昵称
  PostCancelLogin: webUrl + "WxLogin/PostCancelLogin?from=app",//退出登录
  PostEditPassword: webUrl + "WxLogin/PostEditPassword?from=app",//设置密码
  PostSaveIcon: webUrl + "MiniNews/PostSaveIcon?from=app",//设置密码
  PostDeleteNews: webUrl + "MiniNews/PostDeleteNews?from=app",//删除新闻
}